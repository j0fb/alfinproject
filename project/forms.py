from django import forms
# from django.contrib.auth.models import User
from . import models


class WorkerForm(forms.ModelForm):
    areas = forms.MultipleChoiceField(choices=models.AREA_CHOICES, label='Expertise Areas')

    class Meta:
        model = models.Worker
        fields = [
            'location',
            'fee'
        ]


class ProviderForm(forms.ModelForm):

    class Meta:
        model = models.Provider
        fields = ['credit_card']


class NewTaskForm(forms.ModelForm):
    description = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = models.Task
        exclude = ['provider', 'worker', 'started', 'completed']
