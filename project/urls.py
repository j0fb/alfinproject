from django.conf.urls import url
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    # url(r'^$', views.index, name='index'),
    url(r'^$', auth_views.login, {'template_name': 'index.html'}, name='login'),
    url('^logout/$', auth_views.logout, {'next_page': 'login'}),
    url('^signup-worker/$', views.signup_worker, name='signup-worker'),
    url('^worker/$', views.worker_home, name='worker-index'),
    url('^signup-provider/$', views.signup_provider, name='signup-provider'),
    url('^provider/$', views.provider_home, name='provider-index'),
    url('^redirect-home/', views.redirect_home, name='redirect-home'),
    url(r'^provider/(?P<task_id>[0-9]+)/$', views.select_workers, name='select-workers'),
    url(r'^review/(?P<task_id>[0-9]+)/$', views.review_workers, name='review-workers'),
]
