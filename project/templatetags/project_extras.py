from django import template

register = template.Library()


@register.filter
def per_area(str, area):
    return str.split(',')[int(area) - 1]


@register.filter
def per_area_cred(worker, area):
    cred = int(worker.credibility.split(',')[int(area) - 1])
    if cred == 0:
        return 0.5
    else:
        return cred/(float(worker.experience.split(',')[int(area) - 1]) * 10)
