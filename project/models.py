from __future__ import unicode_literals

from django.db import models

# Create your models here.
from django.contrib.auth.models import User

SOFTWARE_DEVELOPER = '1'
SOFTWARE_TESTER = '2'
ELECTRICIAN = '3'
PLUMBER = '4'
CARPENTER = '5'
AREA_CHOICES = (
    (SOFTWARE_DEVELOPER, 'Software Developer'),
    (SOFTWARE_TESTER, 'Software Tester'),
    (ELECTRICIAN, 'Electrician'),
    (PLUMBER, 'Plumber'),
    (CARPENTER, 'Carpenter')
)


class Provider(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    credit_card = models.CharField(max_length=16)

    class Meta:
        permissions = (('is_provider', 'Is a provider'),)

    def __str__(self):
        return self.user.username


class Task(models.Model):
    description = models.CharField(max_length=1000)
    provider = models.OneToOneField(Provider, on_delete=models.CASCADE, null=True)
    location = models.CharField(max_length=100)
    time_limit = models.PositiveIntegerField(verbose_name='No. of days')
    budget = models.PositiveIntegerField()
    area = models.CharField(max_length=1, choices=AREA_CHOICES, verbose_name='Expertise Area')
    started = models.BooleanField(default=False)
    completed = models.BooleanField(default=False)

    def get_workers(self):
        return Worker.objects.filter(
            areas__icontains=self.area).filter(
            availability=True).filter(
            location__iexact=self.location).filter(
            fee__lte=self.budget)


class Worker(models.Model):

    areas = models.CommaSeparatedIntegerField(max_length=100)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    location = models.CharField(max_length=100)
    fee = models.PositiveIntegerField()
    availability = models.BooleanField(default=True)
    experience = models.CharField(max_length=100, default='0,0,0,0,0')
    credibility = models.CharField(max_length=100, default='0,0,0,0,0')
    total_earned = models.PositiveIntegerField(default=0)
    task = models.ForeignKey(Task, on_delete=models.SET_NULL, null=True)

    class Meta:
        permissions = (('is_worker', 'Is a worker'),)

    def __str__(self):
        return self.user.username
