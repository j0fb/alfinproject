# from django.http import HttpResponse

from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.http import Http404


from . import forms
from . import models


def signup_worker(request):
    if request.method == 'POST':
        user_form = UserCreationForm(request.POST)
        worker_form = forms.WorkerForm(request.POST)
        if all((user_form.is_valid(), worker_form.is_valid())):
            user = user_form.save()
            worker = worker_form.save(commit=False)
            areas = worker_form.cleaned_data['areas']
            worker.areas = ','.join(areas)
            worker.user = user
            worker.save()
            content_type = ContentType.objects.get_for_model(models.Worker)
            permission = Permission.objects.get(content_type=content_type, codename='is_worker')
            user.user_permissions.add(permission)
            messages.add_message(request, messages.INFO, 'User created successfully.')
            return redirect(reverse('login'))
    else:
        user_form = UserCreationForm()
        worker_form = forms.WorkerForm()

    return render(
        request,
        'signup.html',
        {'user_form': user_form, 'extra_form': worker_form, 'post_url': 'signup-worker'})


@permission_required('project.is_worker')
def worker_home(request):
    if request.method == 'POST':
        value = request.POST['button']
        worker = request.user.worker
        if value == 'accept':
            worker.availability = False
            worker.save()
            if not any(w.availability for w in worker.task.worker_set.all()):
                task = worker.task
                task.started = True
                task.save()
            messages.add_message(request, messages.INFO, 'Task accepted.')
        elif value == 'reject':
            task = worker.task
            task.provider = None
            task.save()
            for w in worker.task.worker_set.all():
                w.availability = True
                w.task = None
                w.save()
            messages.add_message(request, messages.INFO, 'Task rejected.')
        return redirect(reverse('worker-index'))
    return render(request, 'worker.html', {'worker': request.user.worker})


def signup_provider(request):
    if request.method == 'POST':
        user_form = UserCreationForm(request.POST)
        provider_form = forms.ProviderForm(request.POST)
        if all((user_form.is_valid(), provider_form.is_valid())):
            user = user_form.save()
            provider = provider_form.save(commit=False)
            provider.user = user
            provider.save()
            content_type = ContentType.objects.get_for_model(models.Provider)
            permission = Permission.objects.get(content_type=content_type, codename='is_provider')
            user.user_permissions.add(permission)
            messages.add_message(request, messages.INFO, 'User created successfully.')
            return redirect(reverse('login'))

    else:
        user_form = UserCreationForm()
        provider_form = forms.ProviderForm()

    return render(
        request,
        'signup.html',
        {'user_form': user_form, 'extra_form': provider_form, 'post_url': 'signup-provider'})


@permission_required('project.is_provider')
def provider_home(request):
    try:
        task = request.user.provider.task
        return render(
            request,
            'provider.html',
            {'task': task}
        )
    except models.Task.DoesNotExist:
        if request.method == 'POST':
            new_task_form = forms.NewTaskForm(request.POST)
            if new_task_form.is_valid():
                new_task = new_task_form.save(commit=False)
                new_task.save()
                return redirect(reverse('select-workers', kwargs={'task_id': new_task.id}))
        else:
            new_task_form = forms.NewTaskForm()
        return render(
            request,
            'provider.html',
            {'form': new_task_form}
        )


@login_required
def redirect_home(request):
    if request.user.has_perm('project.is_worker'):
        return redirect(reverse('worker-index'))
    elif request.user.has_perm('project.is_provider'):
        return redirect(reverse('provider-index'))
    else:
        raise Http404


@permission_required('project.is_provider')
def select_workers(request, task_id):
    try:
        task = models.Task.objects.get(pk=task_id)
    except models.Task.DoesNotExist:
        raise Http404("Task does not exist")
    workers = task.get_workers()
    if request.method == 'POST':
        worker_ids = request.POST.getlist('workers[]')
        if not worker_ids or not set(worker_ids).issubset([str(w.id) for w in workers]):
            messages.add_message(request, messages.ERROR, 'Select Workers')
        else:
            for w in worker_ids:
                task.worker_set.add(models.Worker.objects.get(pk=w))
            task.provider = request.user.provider
            task.save()
            return redirect(reverse('provider-index'))
    return render(request, 'select_workers.html', {'workers': workers, 'task': task})


@permission_required('project.is_provider')
def review_workers(request, task_id):
    try:
        task = models.Task.objects.get(pk=task_id)
    except models.Task.DoesNotExist:
        raise Http404("Task does not exist")
    if task.started is False or task.provider != request.user.provider:
        return redirect(reverse('provider-index'))
    worker_ids = [str(w.id) for w in task.worker_set.all()]
    workers = task.worker_set.all()

    def return_error():
        messages.add_message(request, messages.ERROR, 'Review workers')
        return render(request, 'review_workers.html', {'workers': workers, 'task': task})

    if request.method == 'POST':
        if not set(worker_ids).issubset(set(request.POST.keys())):
            print set(worker_ids)
            print set(request.POST.keys())
            return return_error()
        for w in worker_ids:
            try:
                v = request.POST[w]
                if int(v) < 1 or int(v) > 10:
                    print 'b'
                    return return_error()
            except (ValueError, TypeError) as e:
                print 'c'
                return return_error()
        for w in workers:
            rating = int(request.POST[str(w.id)])
            ex = [int(i) for i in w.experience.split(',')]
            ex[int(task.area) - 1] += 1
            w.experience = ','.join([str(i) for i in ex])
            cre = [int(i) for i in w.credibility.split(',')]
            cre[int(task.area) - 1] += rating
            w.credibility = ','.join([str(i) for i in cre])
            w.availability = True
            w.task = None
            w.save()
        task.provider = None
        task.save()
        messages.add_message(request, messages.INFO, 'Task review successful.')
        return redirect(reverse('provider-index'))
    return render(request, 'review_workers.html', {'workers': workers, 'task': task})
