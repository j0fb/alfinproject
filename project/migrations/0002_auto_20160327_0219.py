# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-27 02:19
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0001_squashed_0008_auto_20160326_2259'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='task',
            name='workers',
        ),
        migrations.AlterField(
            model_name='task',
            name='description',
            field=models.CharField(max_length=1000),
        ),
        migrations.AlterField(
            model_name='task',
            name='provider',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='project.Provider'),
        ),
    ]
